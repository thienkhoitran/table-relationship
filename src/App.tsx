import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Container from './container';
import './App.css';
import LoginPage from './container/login';

const App: React.FC = () => {
  return (
    <Router>
      <React.Suspense fallback={<div />}>
        <Switch>
          <Route exact path="/" component={Container} />
          <Route path="/login" component={LoginPage} />
        </Switch>
      </React.Suspense>
    </Router>
  )
}

export default App;
