import React from 'react';
import { IFirbaseTable } from '../../utils/types/table';
import Modal from '../../utils/modal';
import AddDataWareHouseForm from '../add-datawarehouse-form';
import dbImage from'../../../public/images/database.png';
import { IDataWareHouse, IFirbaseDataWareHouse } from '../../utils/types/dataWareHouse';

interface IProps {
  datawareHouses: IFirbaseDataWareHouse[];
  deleteDataWareHouse: (dataWareHouse: IFirbaseDataWareHouse) => boolean;
  addOnBoardTables: (dataWareHouse: IFirbaseDataWareHouse, idx: number) => void;
}

const LeftArea: React.FC<IProps> = ({ datawareHouses, deleteDataWareHouse, addOnBoardTables }) => {
  return (
    <div className="flex-grow flex flex-col bg-white border-t border-b sm:rounded sm:border shadow overflow-hidden">
      <div className="border-b">
        <div className="flex justify-between px-6 -mb-px">
          <h3 className="text-blue-dark py-4 font-normal text-lg">Data warehouses</h3>

          <Modal buttonName='' title='Adding data warehouse' component={<AddDataWareHouseForm />}></Modal>
        </div>
      </div>
      {
        _render(datawareHouses, deleteDataWareHouse, addOnBoardTables)
      }
    </div>
  )
};

const _render = (
  dataWareHouses: IFirbaseDataWareHouse[],
  deleteDataWareHouse: (dataWareHouse: IFirbaseDataWareHouse) => boolean,
  addOnBoardTables: (dataWareHouse: IFirbaseDataWareHouse, idx: number) => void
  ) => {

  if (dataWareHouses.length !== 0) {
    return dataWareHouses.map(
      (dataWareHouse: IFirbaseDataWareHouse, idx: number) => (
        <div className="flex px-6 py-6 text-grey-darker items-center border-b -mx-4" key={dataWareHouse.key}>
        <div className="flex-1 px-4 flex items-center">
          <div className="rounded-full inline-flex mr-3">
            <img  src={dbImage} alt="db"/>
          </div>
          <p className="text-lg ml-15 mx-5">{dataWareHouse.data.dataWareHouseName}</p>
        </div>
        <div className="flex-1">
            <div className="text-right text-grey">
            <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center" onClick={() => {deleteDataWareHouse(dataWareHouse)}}>
              <svg className="fill-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M6 2l2-2h4l2 2h4v2H2V2h4zM3 6h14l-1 14H4L3 6zm5 2v10h1V8H8zm3 0v10h1V8h-1z"/></svg>
            </button>
            <button className="mx-5 bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center" onClick={() => {addOnBoardTables(dataWareHouse, idx)}}>
              <svg className="fill-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M12.95 10.707l.707-.707L8 4.343 6.586 5.757 10.828 10l-4.242 4.243L8 15.657l4.95-4.95z"/></svg>
            </button>
            </div>
        </div>
      </div>
      )
    )
  }
}

export default LeftArea;
