import React from 'react';
import { ITable, IFirbaseTable } from './../utils/types/table';
import { IDiagramExport } from '../utils/types/diagram';
import _filter from 'lodash/filter';
import { errorToast, successToast } from '../utils/toast';
import * as firebase from 'firebase';
import { FirebaseDataBase } from '../utils/types/firebase';
import config from '../utils/firebase/config';
import { putConnection, getConnection, putDataWareHouse, putTable, deleteTable,
   listenDataWareHouseChanges, deleteDataWareHouse } from '../utils/firebase/api';
import { DiagramModel } from 'storm-react-diagrams';
import { IFirbaseDataWareHouse, IDataWareHouse } from './../utils/types/dataWareHouse';
export type addTableType = (table: ITable) => void;
export type deleteTableType = (table: IFirbaseTable) => boolean;
export type onSaveDiagramType = (model: DiagramModel) => void;
export type addTableToDataWareHouseType = (table: ITable) => void;
export type setDataWareHouseType = (data: IDataWareHouse) => void;
export type addDataWareHouseType = (data: IDataWareHouse) => void;
export type deleteDataWareHouseType = (data: IFirbaseDataWareHouse) => boolean;

interface IProps {
};

interface IState {
  datawareHouses: IFirbaseDataWareHouse[];
  dataWareHouse: IDataWareHouse;
  tables: IFirbaseTable[];
  addTable: addTableType;
  addTableToDataWareHouse: addTableToDataWareHouseType;
  addDataWareHouse: addDataWareHouseType;
  deleteTable: deleteTableType;
  diagram: IDiagramExport;
  onSaveDiagram: onSaveDiagramType;
  setDataWareHouse: setDataWareHouseType;
  deleteDataWareHouse: deleteDataWareHouseType;
}

firebase.initializeApp(config);
const firebaseDatabase: FirebaseDataBase = firebase.database();

export const CommonContext = React.createContext<IProps>({});

export function withContext(Component: React.FC<any>) {
  return function ComponentWithContext(props: any) {
    return (
      <CommonContext.Consumer>
        {context => <Component {...props} context={context} />}
      </CommonContext.Consumer>
    );
  };
}

export default class ContextProvider extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props);

    this.state = {
      datawareHouses: [],
      dataWareHouse: {} as IDataWareHouse,
      tables: [],
      addTable: this.addTable.bind(this),
      addTableToDataWareHouse: this.addTableToDataWareHouse.bind(this),
      deleteTable: this.deleteTable.bind(this),
      diagram: {} as IDiagramExport,
      onSaveDiagram: this.onSaveDiagram.bind(this),
      setDataWareHouse: this.setDataWareHouse.bind(this),
      addDataWareHouse: this.addDataWareHouse.bind(this),
      deleteDataWareHouse: this.deleteDataWareHouse.bind(this),

    };
  }

  componentDidMount() {
    listenDataWareHouseChanges(firebaseDatabase, (datawareHouses: IFirbaseDataWareHouse[]) => {
      if (datawareHouses) this.setState({ datawareHouses });
    });

    getConnection(firebaseDatabase).then((diagram: IDiagramExport) => {
      if (diagram) this.setState({ diagram})
    });
  }

  addTableToDataWareHouse = (table: ITable) => {
    const {dataWareHouse} = this.state;
    let {tables} = dataWareHouse;

    if (typeof(tables) === 'undefined') tables = [];

    this.setState({
      dataWareHouse: {
        ...dataWareHouse,
        tables: [...tables, table]
      }
    });
  }

  setDataWareHouse = (data: IDataWareHouse) => {
   this.setState({
     dataWareHouse: data
   })
  };

  addDataWareHouse = (data: IDataWareHouse) => {
    putDataWareHouse(data, firebaseDatabase);
  }

  addTable = (table: ITable) => {
    const { tables } = this.state;
    const isExist = _filter(tables, t => (t.data.tableName === table.tableName));

    if (tables.length !== 0 && isExist.length > 0) {
      errorToast("This table is already exist");
    } else {
      putTable(table, firebaseDatabase);
      successToast("Added new table successfull");
    }
  }

  deleteTable = (table: IFirbaseTable) => {
    if (table.key) {
      deleteTable(table.key, table.data, firebaseDatabase);
      successToast("Remove table successfull");
      return true;
    }

    return false;
  }

  deleteDataWareHouse = (dataWareHouse: IFirbaseDataWareHouse) => {
    if (dataWareHouse.key) {
      deleteDataWareHouse(dataWareHouse.key, firebaseDatabase);
      successToast("Remove datawarehouse successfull");
      return true;
    }

    return false;
  }

  onSaveDiagram = (model: DiagramModel) => {
    putConnection(model.serializeDiagram(), firebaseDatabase);
    successToast("Save diagram successfull");
  }

  render() {
    return (
      <CommonContext.Provider value={this.state}>
        {this.props.children}
      </CommonContext.Provider>
    );
  }
}
