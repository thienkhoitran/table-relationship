import React from 'react';
import ContextProvider from './context';
import Home from './home';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Container: React.FC = () => {
  return (
    <ContextProvider>
      <Home />
      <ToastContainer />
    </ContextProvider>
  )
};

export default Container;
