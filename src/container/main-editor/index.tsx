import React, { useEffect } from 'react';
import { DiagramEngine, DiagramModel, DiagramWidget } from 'storm-react-diagrams';

interface IProps {
  model: DiagramModel;
  engine: DiagramEngine;
  onSaveDiagram: () => void;
  rePaint: number;
};

const MainEditor: React.FC<IProps> = ({ model, engine, onSaveDiagram, rePaint }) => {
  engine.setDiagramModel(model);

  useEffect(
    () => {
      engine.repaintCanvas();
    },
    [rePaint]
  );

  return (
    <>
      <div className="bg-white border-t border-b sm:rounded sm:border shadow">
        <div className="border-b">
          <div className="flex justify-between px-6 -mb-px">
            <h3 className="flex-1 text-blue-dark py-4 font-normal text-lg">Smart Data Cube Dashboard</h3>
            <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
                 onClick={() => engine.zoomToFit() }
                >
                 <span className="h-5"> zoom to fit </span>
                </button>
            <div className="flex-1 py-4">
              <div className="text-right text-grey">

                <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center"
                 onClick={() => onSaveDiagram() }
                >
                  <svg className="fill-current w-5 h-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm0-2a8 8 0 1 0 0-16 8 8 0 0 0 0 16zm-2-8V5h4v5h3l-5 5-5-5h3z" /></svg>
                </button>

              </div>
            </div>
          </div>
        </div>
        <div>
          <DiagramWidget className="bg-gray-800 w-full min-h-750" diagramEngine={engine} maxNumberPointsPerLink={0} allowCanvasZoom={true} inverseZoom={true} />
        </div>
      </div>
    </>
  )
};

export default MainEditor;
