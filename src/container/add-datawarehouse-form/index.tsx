import React, { useState, KeyboardEvent, FormEvent } from 'react';
import { ITableField, ITable } from '../../utils/types/table';
import { withContext } from '../context';
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import { errorToast, successToast } from '../../utils/toast';
import Modal from '../../utils/modal';
import AddTableForm from '../add-table-form';
import { IFirbaseDataWareHouse, IDataWareHouse } from '../../utils/types/dataWareHouse';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import { Button } from '@material-ui/core';

const inforIcon = () => (
  <div className="alert-icon flex items-center bg-blue-100 border-2 border-blue-500 justify-center h-10 w-10 flex-shrink-0 rounded-full">
    <span className="text-blue-500">
      <svg fill="currentColor"
        viewBox="0 0 20 20"
        className="h-6 w-6">
        <path fill-rule="evenodd"
          d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z"
          clip-rule="evenodd"></path>
      </svg>
    </span>
  </div>
)
const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '95%'
    },
  },
}));

const _renderDatabases = (databases: ITable[] = []) => {

  if (databases.length === 0) {
    return (
      <div className="bg-orange-lightest border-l-4 border-orange text-orange-dark p-4" role="alert">
        <p className="font-bold">Warning</p>
        <p>Please add database</p>
      </div>
    )
  } else {
    return _map(databases, (db: ITable, idx: number) => (
      <div key={idx} className="alert my-3 flex flex-row items-center bg-blue-200 p-5 rounded border-b-2 border-blue-300">
        {inforIcon()}
        <div className="alert-content ml-4">
          <div className="alert-title font-semibold text-lg text-blue-800">
            {db.tableName}
          </div>
          <div className="alert-description text-sm text-blue-600">
            Attributes: {
              _map(db.tableFields, (field: ITableField) => (
                <span> [ {field.fieldDataType} - {field.fieldName} ] </span>
              ))
            }
          </div>
        </div>
      </div>

    ))
  }
}

const AddDataWareHouseForm: React.FC = (props: any) => {
  const classes = useStyles();
  const { dataWareHouse, setDataWareHouse } = props.context;

  const submit = () => {
    if (typeof (dataWareHouse.dataWareHouseName) === 'undefined'
      || dataWareHouse.dataWareHouseName === ''
      || typeof (dataWareHouse.tables) === 'undefined'
      || dataWareHouse.tables.length === 0) {
      errorToast(typeof (dataWareHouse.dataWareHouseName) === 'undefined' || dataWareHouse.dataWareHouseName === '' ?
        'Please fill the name of data warehouse' : 'Please fill add at least one database');
    } else {
      props.context.addDataWareHouse(dataWareHouse);

      successToast("Added new table successfull");
      setDataWareHouse({} as IDataWareHouse);
    }
  }

  const _handleOnChange = (name: string) => {
    setDataWareHouse({
      ...dataWareHouse,
      dataWareHouseName: name
    })
  }
  return (
    <div className="flex-grow flex flex-col bg-white overflow-hidden">
      <div className="border-b">
        <div className="mb-4 relative">
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              label="Data warehouse name"
              variant="outlined"
              className="min-w-100"
              value={dataWareHouse.dataWareHouseName || ''}
              onChange={(e) => _handleOnChange(e.currentTarget.value)}
            />
          </form>

        </div>

      </div>
      {_renderDatabases(dataWareHouse.tables as ITable[])}
      <div className="m-auto mb-5">
        <Modal buttonName='Add database' title='Adding database' component={<AddTableForm />} />

      </div>

        <Button
          variant="contained"
          color="primary"
          onClick={submit}
          className="m-auto"
        >
          Save
        </Button>

    </div>
  )
};

export default withContext(AddDataWareHouseForm);
