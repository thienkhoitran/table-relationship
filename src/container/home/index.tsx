import React, { useState, useEffect } from 'react';
import { DiagramModel, DiagramEngine, DefaultNodeModel, DefaultPortModel, DefaultLinkModel } from 'storm-react-diagrams';
import MainEditor from '../main-editor';
import { IFirbaseTable } from '../../utils/types/table';
import { createDataWareHouseLinks, createDataWareHouseModels } from '../../utils/canvas-helpers/node-helper';
import { withContext } from '../context';
import LeftArea from '../left-area';
import Layout from './layout';
import { IHomeProps } from './home-type';
import { IFirbaseDataWareHouse, IDataWareHouse } from '../../utils/types/dataWareHouse';
import _map from 'lodash/map';

const engine: DiagramEngine = new DiagramEngine();
engine.installDefaultFactories();

let model: DiagramModel = new DiagramModel();

const Home: React.FC<IHomeProps> = (props: IHomeProps) => {
  const [currentModel, setCurrentModel] = useState(model);
  const { datawareHouses, deleteDataWareHouse, diagram, onSaveDiagram } = props.context;
  const [rePaint, setRepaint] = useState(0);

  useEffect(() => {
    if (diagram) {
      const newModel: DiagramModel = new DiagramModel();
      newModel.deSerializeDiagram(diagram, engine);
      setCurrentModel(newModel);
    }
  }, [diagram]);

  const addOnBoardTables = (datawareHouse: IFirbaseDataWareHouse, idx: number): void => {

    const theDiagramModel: DiagramModel = currentModel;
    const currentLinks = theDiagramModel.getLinks();

    _map(currentLinks, link => theDiagramModel.removeLink(link));

    const { nodes } = createDataWareHouseModels(datawareHouse, idx)
    _map(nodes, node => theDiagramModel.addNode(node));

    const { links } = createDataWareHouseLinks(theDiagramModel.nodes);
    _map(links, link => theDiagramModel.addLink(link));

    setRepaint(rePaint + 1);
    setCurrentModel(theDiagramModel);
  }

  const saveDiagram = () => {
    onSaveDiagram(currentModel);
  }

  return (
    <Layout
      leftArea={<LeftArea datawareHouses={datawareHouses} deleteDataWareHouse={deleteDataWareHouse} addOnBoardTables={addOnBoardTables} />}
      mainArea={<MainEditor rePaint={rePaint} engine={engine} model={currentModel} onSaveDiagram={() => saveDiagram()} />}
    />
  )
};

export default withContext(Home);
