import { addTableType, deleteTableType, deleteDataWareHouseType ,onSaveDiagramType } from '../context';
import { IFirbaseDataWareHouse } from '../../utils/types/dataWareHouse';

// @flow
export interface IHomeProps {
    context: IContext;
};
export type IContext = {
    datawareHouses: IFirbaseDataWareHouse[];
    diagram: IDiagram;
    addTable: addTableType;
    deleteDataWareHouse: deleteDataWareHouseType;
    onSaveDiagram: onSaveDiagramType;
};
export type ITablesItem = {
    key: string;
    data: IData;
};
export type IData = {
    tableFields: ITableFieldsItem[];
    tableName: string;
    test?: number;
};
export type ITableFieldsItem = {
    fieldDataType: string;
    fieldName: string;
};
export type IDiagram = {
    gridSize: number;
    id: string;
    links: ILinksItem[];
    nodes: INodesItem[];
    offsetX: number;
    offsetY: number;
    zoom: number;
};
export type ILinksItem = {
    color: string;
    curvyness: number;
    id: string;
    points: IPointsItem[];
    selected: boolean;
    source: string;
    sourcePort: string;
    target: string;
    targetPort: string;
    type: string;
    width: number;
};
export type IPointsItem = {
    id: string;
    selected: boolean;
    x: number;
    y: number;
};
export type INodesItem = {
    color: string;
    id: string;
    name: string;
    ports: IPortsItem[];
    selected: boolean;
    type: string;
    x: number;
    y: number;
};
export type IPortsItem = {
    id: string;
    in: boolean;
    label: string;
    links?: string[];
    name: string;
    parentNode: string;
    selected: boolean;
    type: string;
};
