import React from 'react';

interface IProps {
	leftArea: React.ReactNode;
	mainArea: React.ReactNode;
};

const Container: React.FC<IProps> = ({ leftArea, mainArea }) => {
	return (
		<div className="font-sans bg-grey-lighter flex flex-col min-h-screen w-full">
      <div>
        <div className="bg-blue-dark">
          <div className="container mx-auto px-4">
            <div className="flex items-center md:justify-between py-4">
              <div className="w-1/4 md:hidden">
                <svg className="fill-current text-white h-8 w-8" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M16.4 9H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1zm0 4H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1zM3.6 7h12.8c.552 0 .6-.447.6-1 0-.553-.048-1-.6-1H3.6c-.552 0-.6.447-.6 1 0 .553.048 1 .6 1z" /></svg>
              </div>
              <div className="w-1/2 md:w-auto text-center text-white text-2xl font-medium">
                Big data simualation
            </div>
              <div className="w-1/4 md:w-auto md:flex text-right">
                <div>
                  <img className="inline-block h-16" src="https://www.pngfind.com/pngs/m/133-1330095_cray-urika-xc-analytics-software-big-data-analysis.png" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="hidden bg-blue-dark md:block md:bg-white md:border-b">
          <div className="container mx-auto px-4">
            <div className="md:flex">
              <div className="flex -mb-px mr-8">
                <div className="no-underline text-white md:text-blue-dark flex items-center py-4 border-b border-blue-dark">
                  <svg className="h-6 w-6 fill-current mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill-rule="evenodd" d="M3.889 3h6.222a.9.9 0 0 1 .889.91v8.18a.9.9 0 0 1-.889.91H3.89A.9.9 0 0 1 3 12.09V3.91A.9.9 0 0 1 3.889 3zM3.889 15h6.222c.491 0 .889.384.889.857v4.286c0 .473-.398.857-.889.857H3.89C3.398 21 3 20.616 3 20.143v-4.286c0-.473.398-.857.889-.857zM13.889 11h6.222a.9.9 0 0 1 .889.91v8.18a.9.9 0 0 1-.889.91H13.89a.9.9 0 0 1-.889-.91v-8.18a.9.9 0 0 1 .889-.91zM13.889 3h6.222c.491 0 .889.384.889.857v4.286c0 .473-.398.857-.889.857H13.89C13.398 9 13 8.616 13 8.143V3.857c0-.473.398-.857.889-.857z" /></svg>
									Smart Data Cube Dashboard
              </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex-grow container min-w-full mx-auto sm:px-4 pt-6 pb-8">
        <div className="flex flex-wrap -mx-4">
          <div className="w-1/4 mb-6 lg:mb-0 px-4">
							{leftArea}
          </div>
          <div className="w-3/4 mb-6 lg:mb-0 px-4">
            {mainArea}
          </div>
        </div>
      </div>
    </div>
	)
};

export default Container;
