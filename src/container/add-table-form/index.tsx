import React, { useState, KeyboardEvent, FormEvent } from 'react';
import { ITableField, ITable } from '../../utils/types/table';
import { withContext } from '../context';
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import { errorToast, successToast } from '../../utils/toast';
import { TextField, makeStyles, MenuItem, Button } from '@material-ui/core';
const fields: ITableField[] = [{
  fieldDataType: 'string',
  fieldName: ''
}];

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
  },
}));

const types = [
  {
    value: 'string',
    label: 'string',
  },
  {
    value: 'number',
    label: 'number',
  },
  {
    value: 'boolean',
    label: 'boolean',
  },
]
const AddTableForm: React.FC = (props: any) => {
  const [table, setTable] = useState({} as ITable);
  const classes = useStyles();

  const submit = () => {

    const isValidFields = _filter(table.tableFields, field => (field.fieldName !== ''));

    if (table.tableName === '' || isValidFields.length === 0) {
      errorToast( typeof(table.tableName) === 'undefined' || table.tableName === '' ?
       'Please fill the name of table' : 'Please fill at least one field');
    } else {
      props.context.addTableToDataWareHouse(table);

      successToast("Added new table successfull");
      setTable({} as ITable);
    }
  }

  const renderFields = () => {
    if (typeof (table.tableFields) === 'undefined') {
      setTable({
        ...table,
        tableFields: [
          {
            fieldName: '',
            fieldDataType: 'string'
          }
        ]
      })
    }
    const { tableFields } = table;

    return _map(tableFields, (field: ITableField, idx: number) => {
      return (
        <div className="flex text-sm" key={idx}>
          <TextField
            select
            label="Select"
            value={field.fieldDataType}
            onChange={(e) => _handleOnChangesTableField(idx, 'fieldDataType', e.target.value)}
            variant="outlined"
            className="w-130px"
          >
            {types.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </TextField>
          <TextField
            className="w-65percent"
            label="Field name"
            variant="outlined"
            name={'fieldName' + idx}
            value={field.fieldName}
            onChange={(e) => _handleOnChangesTableField(idx, 'fieldName', e.currentTarget.value)}
            onKeyPress={(e: KeyboardEvent<HTMLDivElement>) => _handlePressEnter(e)}
          />

        </div>
      )
    })

  }

  const _handlePressEnter = (e: KeyboardEvent<HTMLDivElement>) => {
    if (e.charCode === 13) {
      const newField: ITableField = {
        fieldDataType: 'string',
        fieldName: ''
      }

      setTable({
        ...table,
        tableFields: [...table.tableFields, newField]
      });
    }
  }

  const _handleOnChanges = (attribute: string, value: string) => {
    setTable({
      ...table,
      [attribute]: value
    })
  }

  const _handleOnChangesTableField = (idx: number, attribute: string, value: string) => {
    let { tableFields } = table;

    tableFields[idx] = {
      ...tableFields[idx],
      [attribute]: value
    }

    setTable({
      ...table,
      tableFields
    })
  }

  return (
    <div className="form">
      <div className={classes.root}>
        <TextField
          label="Database name"
          variant="outlined"
          className="w-95percent"
          value={table.tableName || ''}
          onChange={(e) => _handleOnChanges('tableName', e.currentTarget.value)}
        />
        <div className="flex">
          <h5 className="text-blue-dark py-4 font-normal text-lg">Attributes</h5>
        </div>
        {renderFields()}

        <div className="submit w-100 text-center">
          {/* <button
            className="w-1/2 bg-blue-600 shadow-lg text-white px-4 py-2 hover:bg-blue-700 mt-8 text-center font-semibold focus:outline-none "
          >Submit</button> */}
           <Button
            variant="contained"
            color="primary"
            onClick={submit}
            className="m-auto"
          >
            Save
          </Button>
        </div>
      </div>

    </div>
  )
};

export default withContext(AddTableForm);
