export type FirebaseDataBase = firebase.database.Database;
export type FirebaseSnapshot = firebase.database.DataSnapshot;
