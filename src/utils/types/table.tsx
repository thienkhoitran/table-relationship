export interface ITableField {
  fieldName: string;
  fieldDataType: string;
}

export interface ITable {
  tableName: string;
  tableFields: ITableField[]
};

export interface IFirbaseTable {
  key: string;
  data: ITable;
};
