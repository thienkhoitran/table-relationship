import { ITable } from "./table";

export interface IDataWareHouse {
	dataWareHouseName: string;
	tables: ITable[];
};

export interface IFirbaseDataWareHouse {
	key: string;
	data: IDataWareHouse;
};
