export type IDiagramExport = {
  id: string;
} & {
  offsetX: number;
  offsetY: number;
  zoom: number;
  gridSize: number;
  links: ({
    id: string;
  } & {
    type: string;
    selected: boolean;
  } & {
    source: string;
    sourcePort: string;
    target: string;
    targetPort: string;
    points: ({
      id: string;
    } & {
      type: string;
      selected: boolean;
    } & {
      x: number;
      y: number;
    })[];
    extras: {};
    labels: ({
      id: string;
    } & {
      type: string;
      selected: boolean;
    } & {
      offsetX: number;
      offsetY: number;
    })[];
  })[];
  nodes: ({
    id: string;
  } & {
    type: string;
    selected: boolean;
  } & {
    x: number;
    y: number;
    extras: any;
    ports: ({
      id: string;
    } & {
      type: string;
      selected: boolean;
    } & {
      name: string;
      parentNode: string;
      links: string[];
      maximumLinks: number;
    })[];
  })[];
};
