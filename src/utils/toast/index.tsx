import { toast } from "react-toastify";

const successToast = (msg: String) => {
	toast.success(msg, {
		position: "bottom-center",
		autoClose: 5000,
		hideProgressBar: true,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: false,
		progress: undefined,
	});
}

const errorToast = (msg: String) => {
	toast.error(msg, {
		position: "bottom-center",
		autoClose: 5000,
		hideProgressBar: true,
		closeOnClick: true,
		pauseOnHover: true,
		draggable: false,
		progress: undefined,
	});
}

export {
	successToast,
	errorToast
};