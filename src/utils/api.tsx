import axios, { AxiosResponse } from 'axios';
import { getBearerToken } from './authenticate/login-helper';

const BASE_URL = 'https://tkstudiovn-eval-prod.apigee.net/mxd-api';

const getAPI = async (endpoint: string, params: any): Promise<AxiosResponse> => {
  const token: string = await getBearerToken();

  return axios.get(endpoint, {
    baseURL: BASE_URL,
    params,
    headers: {
      Authorization: `Bearer ${token}`
    }
  })
};

export {
  getAPI
};
