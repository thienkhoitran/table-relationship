import { FirebaseDataBase, FirebaseSnapshot } from '../types/firebase';
import { ITable, IFirbaseTable } from '../types/table';
import { IDiagramExport } from '../types/diagram';
import { map as _map } from 'lodash';
import { IDataWareHouse, IFirbaseDataWareHouse } from '../types/dataWareHouse';

const _firebaseWrapper = (obj: any) => {
  Object.keys(obj).forEach(function (key) {
    const value = obj[key];
    const type = typeof value;
    if (type === 'object') {
      _firebaseWrapper(value);
      if (!Object.keys(value).length) {
        delete obj[key]
      }
    }
    else if (type === 'undefined') {
      delete obj[key];
    }
  });

  return obj;
}

const _putData = (database: FirebaseDataBase, treeName: string, data: any) => {
  const dataToFirebase: any = _firebaseWrapper(data);
  database.ref(`/${treeName}`).push(dataToFirebase);
};

const _setData = (database: FirebaseDataBase, treeName: string, data: any) => {
  const dataToFirebase: any = _firebaseWrapper(data);
  database.ref(`/${treeName}`).set(dataToFirebase);
};

const putTable = (tableData: ITable, database: FirebaseDataBase): void => {
  _putData(database, 'tables', tableData);
};

const putDataWareHouse = (dataWareHouse: IDataWareHouse, database: FirebaseDataBase): void => {
  _putData(database, 'dataWareHouses', dataWareHouse);
};

const putConnection = (connectionModel: IDiagramExport, database: FirebaseDataBase): void => {
  _setData(database, 'connections', connectionModel);
};

const getConnection = (database: FirebaseDataBase): Promise<IDiagramExport> => (
  new Promise((resolve) => {
    database.ref('/connections').once('value').then((snapshot: FirebaseSnapshot) => {
      const value: any = snapshot.val();

      if (value) {
        resolve(value);
      } else {
        resolve(undefined)
      }
    })
  })
);

const listenTableChanges = (database: FirebaseDataBase, onNewDataCome: any): void => {
  database.ref('/tables').on('value', (snapshot: FirebaseSnapshot): void => {
    // transform to array
    onNewDataCome(_map(snapshot.val(), (data: ITable, key: string): IFirbaseTable => ({
      key,
      data
    })));
  })
};

const listenDataWareHouseChanges = (database: FirebaseDataBase, onNewDataCome: any): void => {
  database.ref('/dataWareHouses').on('value', (snapshot: FirebaseSnapshot): void => {
    // transform to array
    onNewDataCome(_map(snapshot.val(), (data: IDataWareHouse, key: string): IFirbaseDataWareHouse => ({
      key,
      data
    })));
  })
};

const updateTable = (tableKey: string, tableData: ITable, database: FirebaseDataBase): void => {
  database.ref(`/tables/${tableKey}`).update(tableData);
};

const deleteTable = (tableKey: string, tableData: ITable, database: FirebaseDataBase): void => {
  database.ref(`/tables/${tableKey}`).remove();
};

const deleteDataWareHouse = (key: string, database: FirebaseDataBase): void => {
  database.ref(`/dataWareHouses/${key}`).remove();
};
export {
  putTable,
  putConnection,
  getConnection,
  listenTableChanges,
  updateTable,
  deleteTable,
  putDataWareHouse,
  listenDataWareHouseChanges,
  deleteDataWareHouse
};
