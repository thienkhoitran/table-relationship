import { DefaultNodeModel, DefaultPortModel, DefaultLinkModel, LinkModel, NodeModel } from 'storm-react-diagrams';
import { forEach as _forEach, map as _map, intersection as _intersection, filter as _filter } from 'lodash';
import randomColor from 'randomcolor';
import { ITable, ITableField } from '../types/table';
import { IDataWareHouse, IFirbaseDataWareHouse } from '../types/dataWareHouse';
import { table } from 'console';

const createNewNode = (table: ITable, x: number = 0, y: number = 100): DefaultNodeModel => {
  const node: DefaultNodeModel = new DefaultNodeModel(table.tableName, randomColor());

  node.setPosition(x, y);
  _forEach(table.tableFields, (field: ITableField) => {
    node.addInPort(field.fieldName);
    node.addOutPort(`(${field.fieldDataType})`);
  });

  return node;
};

const createDataWareHouseModels = (dataWareHouse: IFirbaseDataWareHouse, index: number)=> {

  const x = index*(index % 2 ? 850 : 1);
  const y = index*(index % 2 ? 1 : 300);

  const tables = dataWareHouse.data.tables;

  let nodes = [] as DefaultNodeModel[];

  if (typeof (tables) !== 'undefined' && tables.length !== 0) {
    const mainTable = _findMainTable(tables);

    const normalTable = _filter(tables, table => table.tableName !== mainTable.tableName);

    nodes = _map(normalTable, (table, idx) => {
    let nextX = x;
    let nextY = y;

    if (idx !== 0) {
      switch (idx) {
        case 1:
          nextX += 450;
          break;
        case 2:
          nextY += 450;
          break;
        case 3:
          nextX += 450;
          nextY += 450;
          break;
        default:
          nextX = Math.random()*x;
          nextY = Math.random()*y;
          break;
      }
    }

    return createNewNode(table, nextX, nextY);
    });

    nodes.push(createNewNode(mainTable, x + 175, y + 175));
  }

  return { nodes }
}

const createDataWareHouseLinks = (nodesString: { [s: string]: NodeModel }) => {
  let links = [] as LinkModel[];
  let nodes = [] as DefaultNodeModel[];

  nodes = Object.keys(nodesString).map(
    key => nodesString[key] as DefaultNodeModel
  );

  _forEach(nodes, (node: DefaultNodeModel, idx: number) => {
    if (idx !== nodes.length -1) {
      const inPorts: DefaultPortModel[] = node.getInPorts();

      _forEach(nodes, (nextNode: DefaultNodeModel, nextIdx: number) => {
        if (nextIdx > idx) {
          let count = 0;

          const nextInPorts: DefaultPortModel[] = nextNode.getInPorts();
          const nextOutPorts: DefaultPortModel[] = nextNode.getOutPorts();

          if (node.name !== nextNode.name) {
            _forEach(inPorts, (port: DefaultPortModel) => {
              _forEach(nextInPorts, (nextPort: DefaultPortModel, nextPortIdx: number) => {

                if(port.label === nextPort.label && count !== 1) {
                  const link: DefaultLinkModel = port.link(nextOutPorts[nextPortIdx]) as DefaultLinkModel;
                  link.setColor('#CE169D');
                  links.push(link);
                  count = 1;
                }
              })
            })
          }
        }
      })
    }
  })

  return { links }
}

const _findMainTable = (tables: ITable[]) => {
  let count = 0;
  let mainTable: ITable = {} as ITable;

  _forEach(tables, (table, idx) => {
    let tableCount = 0;

    _forEach(tables, (nextTable, nextIdx) => {
      if (idx !== nextIdx) {
        const fieldsName = _map(table.tableFields, field => field.fieldName );
        const nextFieldsName = _map(nextTable.tableFields, field => field.fieldName );
        if (_intersection(fieldsName, nextFieldsName).length > 0) tableCount++;
      }
    })

    if(tableCount > count) {
      count = tableCount;
      mainTable = table;
    }
  })

  return mainTable;
}
export {
  createNewNode,
  createDataWareHouseModels,
  createDataWareHouseLinks
};
