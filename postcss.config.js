const tailwindcss = require('tailwindcss');
const cssnano = require('cssnano');
const purgecss = require('@fullhuman/postcss-purgecss')({
  content: [
    './src/**/*.tsx'
  ],
  whitelist: ['body'],
  whitelistPatterns: [/^srd/],
  defaultExtractor: content => content.match(/[\w-/:]+(?<!:)/g) || []
});

module.exports = {
  plugins: [
    tailwindcss('./tailwind.config.js'),
    require('autoprefixer'),
    ...process.env.NODE_ENV === 'production'
      ? [
        purgecss,
        cssnano({
          preset: ['default', {
            discardComments: {
              removeAll: true,
            },
          }]
        })]
      : []
  ],
};
